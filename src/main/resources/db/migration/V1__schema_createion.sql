CREATE
    SEQUENCE user_sequence START 1 INCREMENT 1;

CREATE
    SEQUENCE equipment_sequence START 1 INCREMENT 1;

CREATE
    TYPE user_department AS enum(
        'HR',
        'DEVELOPMENT',
        'PRODUCT'
    );

CREATE
    TYPE user_role AS enum(
        'ADMIN',
        'USER'
    );

CREATE
    TYPE user_status AS enum(
        'ENABLED',
        'DISABLED'
    );

CREATE
    TYPE equipment_type AS enum(
        'LAPTOP',
        'TOWER',
        'KEYBOARD',
        'MOUSE',
        'MONITOR'
    );

CREATE
    TYPE equipment_condition AS enum(
        'DAMAGED',
        'VERY_GOOD',
        'MINT'
    );

CREATE
    TABLE
        "user"(
            id int8 NOT NULL,
            username VARCHAR(255) UNIQUE,
            password VARCHAR(255),
            firstname VARCHAR(255),
            lastname VARCHAR(255),
            department user_department,
            status user_status,
            ROLE user_role,
            PRIMARY KEY(id)
        );

CREATE
    TABLE
        EQUIPMENT(
            id int8 NOT NULL,
            name VARCHAR(255),
            TYPE equipment_type,
            CONDITION equipment_condition,
            user_id int8 NOT NULL,
            PRIMARY KEY(id)
        );

ALTER TABLE
    IF EXISTS EQUIPMENT ADD CONSTRAINT user_equipment_id_fk01 FOREIGN KEY(user_id) REFERENCES "user" ON
    DELETE
        CASCADE;
