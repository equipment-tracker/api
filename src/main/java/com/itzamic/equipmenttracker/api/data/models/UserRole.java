package com.itzamic.equipmenttracker.api.data.models;

public enum UserRole {
  ADMIN,
  USER
}
