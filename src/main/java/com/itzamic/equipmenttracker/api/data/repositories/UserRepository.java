package com.itzamic.equipmenttracker.api.data.repositories;

import com.itzamic.equipmenttracker.api.data.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {}
