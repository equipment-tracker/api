package com.itzamic.equipmenttracker.api.data.repositories;

import com.itzamic.equipmenttracker.api.data.models.Equipment;
import org.springframework.data.repository.CrudRepository;

public interface EquipmentRepository extends CrudRepository<Equipment, Long> {}
