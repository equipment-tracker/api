package com.itzamic.equipmenttracker.api.data.models;

import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Equipment {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence-generator")
  @GenericGenerator(
      name = "sequence-generator",
      strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
      parameters = {
        @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "equipment_sequence"),
        @Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1"),
        @Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1")
      })
  private Long id;

  private String name;

  @Enumerated(value = EnumType.STRING)
  private EquipmentType type;

  @Enumerated(value = EnumType.STRING)
  private EquipmentCondition condition;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;
}
