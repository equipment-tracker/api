package com.itzamic.equipmenttracker.api.data.models;

public enum EquipmentCondition {
  DAMAGED,
  VERY_GOOD,
  MINT
}
