package com.itzamic.equipmenttracker.api.data.models;

public enum UserStatus {
  ENABLED,
  DISABLED
}
