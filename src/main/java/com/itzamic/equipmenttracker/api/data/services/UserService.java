package com.itzamic.equipmenttracker.api.data.services;

import com.itzamic.equipmenttracker.api.data.models.User;

public interface UserService extends CrudService<User, Long> {}
