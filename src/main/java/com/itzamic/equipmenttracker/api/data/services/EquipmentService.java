package com.itzamic.equipmenttracker.api.data.services;

import com.itzamic.equipmenttracker.api.data.models.Equipment;

public interface EquipmentService extends CrudService<Equipment, Long> {}
