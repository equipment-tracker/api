package com.itzamic.equipmenttracker.api.data.services;

import com.itzamic.equipmenttracker.api.data.models.User;
import java.util.List;

public class UserServiceImpl implements UserService {
  @Override
  public List<User> findAll() {
    return null;
  }

  @Override
  public User findById(Long aLong) {
    return null;
  }

  @Override
  public User save(User object) {
    return null;
  }

  @Override
  public void delete(User object) {}

  @Override
  public void deleteById(Long aLong) {}
}
