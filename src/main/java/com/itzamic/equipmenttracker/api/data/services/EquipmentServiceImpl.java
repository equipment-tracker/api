package com.itzamic.equipmenttracker.api.data.services;

import com.itzamic.equipmenttracker.api.data.models.Equipment;
import java.util.List;

public class EquipmentServiceImpl implements EquipmentService {
  @Override
  public List<Equipment> findAll() {
    return null;
  }

  @Override
  public Equipment findById(Long aLong) {
    return null;
  }

  @Override
  public Equipment save(Equipment object) {
    return null;
  }

  @Override
  public void delete(Equipment object) {}

  @Override
  public void deleteById(Long aLong) {}
}
