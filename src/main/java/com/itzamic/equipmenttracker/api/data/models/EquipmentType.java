package com.itzamic.equipmenttracker.api.data.models;

public enum EquipmentType {
  LAPTOP,
  TOWER,
  KEYBOARD,
  MOUSE,
  MONITOR
}
