package com.itzamic.equipmenttracker.api.data.models;

public enum UserDepartment {
  HR,
  DEVELOPMENT,
  PRODUCT
}
