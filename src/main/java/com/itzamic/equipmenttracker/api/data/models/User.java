package com.itzamic.equipmenttracker.api.data.models;

import java.util.List;
import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence-generator")
  @GenericGenerator(
      name = "sequence-generator",
      strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
      parameters = {
        @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "user_sequence"),
        @Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1"),
        @Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1")
      })
  private Long id;

  private String username;
  private String password;
  private String firstname;
  private String lastname;

  @Enumerated(value = EnumType.STRING)
  private UserDepartment department;

  @Enumerated(value = EnumType.STRING)
  private UserStatus status;

  @Enumerated(value = EnumType.STRING)
  private UserRole role;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
  private List<Equipment> equipments;
}
