package com.itzamic.equipmenttracker.api.ws.users.controllers;

import com.itzamic.equipmenttracker.api.ws.common.EndpointConstants;
import com.itzamic.equipmenttracker.api.ws.common.resources.PatchOperationWrapperResource;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserResource;
import com.itzamic.equipmenttracker.api.ws.users.services.UserService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointConstants.ADMIN_API_ENDPOINT)
@RequiredArgsConstructor
public class AdminController {
  private final UserService service;

  @GetMapping("/users")
  public ResponseEntity<List<UserResource>> getUsers() {
    return service.getUsers();
  }

  @PatchMapping("/users/{userId}")
  public ResponseEntity<UserResource> updateUser(
      @PathVariable Long userId, @RequestBody PatchOperationWrapperResource resource) {
    return service.updateUser(userId, resource);
  }

  @DeleteMapping("/users/{userId}")
  public ResponseEntity<Void> deleteUser(@PathVariable Long userId) {
    service.deleteUser(userId);
    return ResponseEntity.noContent().build();
  }
}
