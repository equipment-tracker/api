package com.itzamic.equipmenttracker.api.ws.users.converters;

import com.itzamic.equipmenttracker.api.data.models.User;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserResource;
import java.util.function.Function;
import org.springframework.stereotype.Component;

@Component
public class UserToUserResourceFunction implements Function<User, UserResource> {
  @Override
  public UserResource apply(User user) {
    return UserResource.builder().build();
  }
}
