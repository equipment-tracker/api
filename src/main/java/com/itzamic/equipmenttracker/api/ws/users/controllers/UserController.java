package com.itzamic.equipmenttracker.api.ws.users.controllers;

import com.itzamic.equipmenttracker.api.ws.common.EndpointConstants;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserCredentials;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserResource;
import com.itzamic.equipmenttracker.api.ws.users.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointConstants.USER_API_ENDPOINT)
@RequiredArgsConstructor
public class UserController {
  private final UserService service;

  @PostMapping("/login")
  public ResponseEntity<UserResource> login(@RequestBody UserCredentials userCredentials) {
    return service.login(userCredentials);
  }

  @PostMapping("/register")
  public ResponseEntity<UserResource> register(@RequestBody UserResource resource) {
    return service.register(resource);
  }

  @PostMapping("/logout")
  public ResponseEntity<Void> logout() {
    service.logout();
    return ResponseEntity.noContent().build();
  }

  @GetMapping("{userId}/equipment")
  public ResponseEntity<UserResource> getEquipment(@PathVariable Long userId) {
    return service.getEquipment(userId);
  }

  //  private final UserResourceToUserFunction userResourceToUserFunction;
  //  private final UserToUserResourceFunction userToUserResourceFunction;
  //
  //  @PostMapping
  //  public ResponseEntity<UserResource> register(
  //      @RequestBody UserResource resource, UriComponentsBuilder uriBuilder) {
  //    var created = service.save(userResourceToUserFunction.apply(resource));
  //    return ResponseEntity.created(
  //            uriBuilder
  //                .path(EndpointConstants.USER_API_ENDPOINT)
  //                .path("/{userid}")
  //                .build(created.getId()))
  //        .body(userToUserResourceFunction.apply(created));
  //  }
  //
  //  @GetMapping
  //  public ResponseEntity<List<UserResource>> getUsers() {
  //    List<User> users = service.findAll();
  //    return ResponseEntity.ok(
  //        users.stream().map(userToUserResourceFunction).collect(Collectors.toList()));
  //  }
  //
  //  @PatchMapping("/{userid}")
  //  public ResponseEntity<UserResource> updateUser(@PathVariable("userid") final Long userId,
  // @RequestBody PatchOperationWrapperResource payload) {
  //
  //    service.save()
  //    return ResponseEntity.ok(
  //
  //    )
  //  }
}
