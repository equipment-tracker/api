package com.itzamic.equipmenttracker.api.ws.users.converters;

import com.itzamic.equipmenttracker.api.data.models.User;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserResource;
import java.util.function.Function;
import org.springframework.stereotype.Component;

@Component
public class UserResourceToUserFunction implements Function<UserResource, User> {
  @Override
  public User apply(UserResource userResource) {
    return User.builder().build();
  }
}
