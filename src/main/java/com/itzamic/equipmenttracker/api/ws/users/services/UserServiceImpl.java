package com.itzamic.equipmenttracker.api.ws.users.services;

import com.itzamic.equipmenttracker.api.ws.common.resources.PatchOperationWrapperResource;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserCredentials;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserResource;
import java.util.List;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
  private final com.itzamic.equipmenttracker.api.data.services.UserService dataService;
  private final HttpSession session;

  @Override
  public ResponseEntity<UserResource> login(UserCredentials userCredentials) {
    //        if (session.getAttribute(WsLoggedInAuthentication.LOGIN_SESSION_ATTRIBUTE) != null) {
    //            throw new GenericApiException("User already logged-in", HttpStatus.BAD_REQUEST);
    //        }
    //        UserResource result = null;
    //        if (StringUtils.isNotEmpty(userCredentials.getUsername()) &&
    // StringUtils.isNotEmpty(userCredentials.getPassword())) {
    //            User user = dataService.findByUsername(userCredentials.getUsername());
    //            if (user.isStatus())
    //        }
    return null;
  }

  @Override
  public ResponseEntity<UserResource> register(UserResource resource) {
    return null;
  }

  @Override
  public void logout() {}

  @Override
  public ResponseEntity<UserResource> getEquipment(Long id) {
    return null;
  }

  @Override
  public ResponseEntity<List<UserResource>> getUsers() {
    return null;
  }

  @Override
  public ResponseEntity<UserResource> updateUser(
      Long userId, PatchOperationWrapperResource resource) {
    return null;
  }

  @Override
  public void deleteUser(Long userId) {}
}
