package com.itzamic.equipmenttracker.api.ws.equipments.services;

import com.itzamic.equipmenttracker.api.ws.equipments.resources.EquipmentResource;
import java.util.List;
import org.springframework.http.ResponseEntity;

public interface EquipmentService {
  ResponseEntity<List<EquipmentResource>> getEquipment();

  ResponseEntity<EquipmentResource> create(EquipmentResource resource);

  void delete(Long equipmentId);
}
