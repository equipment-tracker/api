package com.itzamic.equipmenttracker.api.ws.equipments.controllers;

import com.itzamic.equipmenttracker.api.ws.common.EndpointConstants;
import com.itzamic.equipmenttracker.api.ws.equipments.resources.EquipmentResource;
import com.itzamic.equipmenttracker.api.ws.equipments.services.EquipmentService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointConstants.EQUIPMENT_API_ENDPOINT)
@RequiredArgsConstructor
public class EquipmentController {
  private final EquipmentService service;

  @GetMapping
  public ResponseEntity<List<EquipmentResource>> getEquipment() {
    return service.getEquipment();
  }

  @PostMapping
  public ResponseEntity<EquipmentResource> create(@RequestBody EquipmentResource resource) {
    return service.create(resource);
  }

  @DeleteMapping("/{equipmentId")
  public ResponseEntity<Void> delete(@PathVariable Long equipmentId) {
    service.delete(equipmentId);
    return ResponseEntity.noContent().build();
  }
}
