package com.itzamic.equipmenttracker.api.ws.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EndpointConstants {
  public static final String USER_API_ENDPOINT = "/api/v1/user";
  public static final String ADMIN_API_ENDPOINT = "/api/v1/admin";
  public static final String EQUIPMENT_API_ENDPOINT = "/api/v1/admin/equipment";
}
