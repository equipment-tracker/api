package com.itzamic.equipmenttracker.api.ws.users.resources;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCredentials {
  private String username;
  private CharSequence password;
}
