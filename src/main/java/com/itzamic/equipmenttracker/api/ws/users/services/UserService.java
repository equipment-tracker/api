package com.itzamic.equipmenttracker.api.ws.users.services;

import com.itzamic.equipmenttracker.api.ws.common.resources.PatchOperationWrapperResource;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserCredentials;
import com.itzamic.equipmenttracker.api.ws.users.resources.UserResource;
import java.util.List;
import org.springframework.http.ResponseEntity;

public interface UserService {
  ResponseEntity<UserResource> login(UserCredentials userCredentials);

  ResponseEntity<UserResource> register(UserResource resource);

  void logout();

  ResponseEntity<UserResource> getEquipment(Long id);

  ResponseEntity<List<UserResource>> getUsers();

  ResponseEntity<UserResource> updateUser(Long userId, PatchOperationWrapperResource resource);

  void deleteUser(Long userId);
}
